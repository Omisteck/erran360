<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ParcelController;
use App\Http\Controllers\RiderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ServiceProviderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix("v1")->group(function(){

    // Route::post('/register', [])
    Route::prefix('/provider')->group(function(){
        Route::get("/", [ServiceProviderController::class, 'index']);
        Route::get("/{service_provider}", [ServiceProviderController::class, 'show']);
        Route::post("/", [ServiceProviderController::class, 'store']);
        Route::delete("/{service_provider}", [ServiceProviderController::class, 'destroy']);
        Route::put("/{service_provider}", [ServiceProviderController::class, 'update']);
    });

    Route::prefix('/customer')->group(function(){
        Route::get("/", [CustomerController::class, 'index']);
        Route::get("/{customer}", [CustomerController::class, 'show']);
        Route::post("/", [CustomerController::class, 'store']);
        Route::delete("/{customer}", [CustomerController::class, 'destroy']);
        Route::put("/{customer}", [CustomerController::class, 'update']);
    });

    Route::prefix('/rider')->group(function(){
        Route::get("/", [RiderController::class, 'index']);
        Route::get("/{rider}", [RiderController::class, 'show']);
        Route::post("/", [RiderController::class, 'store']);
        Route::delete("/{rider}", [RiderController::class, 'destroy']);
        Route::put("/{rider}", [RiderController::class, 'update']);
    });


    Route::prefix('/parcel')->group(function(){
        Route::get("/", [ParcelController::class, 'index']);
        Route::get("/{parcel}", [ParcelController::class, 'show']);
        Route::post("/", [ParcelController::class, 'store']);
        Route::delete("/{parcel}", [ParcelController::class, 'destroy']);
        Route::put("/{parcel}", [ParcelController::class, 'update']);
    });

});