<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreParcelRequest;
use App\Http\Requests\UpdateParcelRequest;
use App\Models\Parcel;

class ParcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parcels = Parcel::all();

        return response()->json($parcels, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreParcelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreParcelRequest $request)
    {
        $store = Parcel::create($request->only([
            'product', 'weight', 'cost','pickup','destination','customer_id','service_provider_id',
        ]));

        return response()->json($store, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Parcel  $parcel
     * @return \Illuminate\Http\Response
     */
    public function show(Parcel $parcel)
    {
        return response()->json($parcel, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Parcel  $parcel
     * @return \Illuminate\Http\Response
     */
    public function edit(Parcel $parcel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateParcelRequest  $request
     * @param  \App\Models\Parcel  $parcel
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateParcelRequest $request, Parcel $parcel)
    {
        $update = $parcel->update($request->only([
            'product', 'weight', 'cost','pickup','destination','customer_id','service_provider_id', "rider_id",
        ]));

        if($update){
            return response()->json(['message' => $parcel->id." updated successfully"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Parcel  $parcel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Parcel $parcel)
    {
        $parcel->delete();

        return response()->json(['message' => 'Parcel deleted successfully!'], 200);
    }
}
